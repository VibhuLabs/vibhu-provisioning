#!/bin/sh
docker stop bottledwater
docker stop postgres
docker stop schema-registry
docker stop kafka
docker stop zookeeper
