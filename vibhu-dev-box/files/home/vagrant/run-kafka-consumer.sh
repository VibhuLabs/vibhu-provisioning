#!/bin/sh

docker run -it --rm --link zookeeper:zookeeper --link kafka:kafka --link schema-registry:schema-registry confluent/tools kafka-avro-console-consumer --property print.key=true --topic test --from-beginning

