docker run -d -p 2181:2181 --name zookeeper --hostname zookeeper confluent/zookeeper
docker run -d -p 9092:9092 --name kafka --hostname kafka --link zookeeper:zookeeper --env KAFKA_LOG_CLEANUP_POLICY=compact confluent/kafka
docker run -d -p 8081:8082 --name schema-registry --hostname schema-registry --link zookeeper:zookeeper --link kafka:kafka --env SCHEMA_REGISTRY_AVRO_COMPATIBILITY_LEVEL=none confluent/schema-registry
docker run -d -p 5432:5432 --name postgres --hostname postgres confluent/postgres-bw:0.1
docker run -it --rm --link postgres:postgres postgres:9.4 sh -c 'exec psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -U postgres -c "create extension bottledwater;"'
docker run -d --name bottledwater --hostname bottledwater --link postgres:postgres --link kafka:kafka --link schema-registry:schema-registry confluent/bottledwater:0.1
docker run -d -t -P --name spark_master epahomov/docker-spark /start-master.sh
docker pull confluent/tools

