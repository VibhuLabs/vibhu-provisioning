#!/bin/sh
docker start zookeeper
docker start kafka
sleep 8
docker start schema-registry
docker start postgres
docker start bottledwater
