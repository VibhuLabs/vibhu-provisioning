#!/bin/sh

if brew info; then
  echo "'brew' is already installed..."
else
  echo "'brew' not found. Installing brew package manager"
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

if brew list -1 | grep -q "^ansible\$"; then
  echo "'ansible' is already installed..."
else
  echo "'ansible' is not installed"
  brew install ansible
fi

vagrant up

ansible-playbook site.yml
